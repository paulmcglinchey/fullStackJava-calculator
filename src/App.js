import { Fragment, useState, useEffect } from "react"
import InputButton from "./components/InputButton"

const App = (props) => {

  const [operator, setOperator] = useState(null);
  const [operatorName, setOperatorName] = useState(null);
  const [prev, setPrev] = useState(null);
  const [current, setCurrent] = useState(null);
  const [calcCurrent, setCalcCurrent] = useState(null);
  const [calcPrev, setCalcPrev] = useState(null);
  const [calcOperator, setCalcOperator] = useState(null);
  const [result, setResult] = useState(null);

  useEffect(() => {
    setResult(prev);
  }, [prev])

  const reset = () => {
    setOperator(null);
    setPrev(null);
    setCurrent(null);
    setResult(null);
    setCalcCurrent(null);
    setCalcPrev(null);
  }

  const divide = () => {
    setPrev(prev / current);
  }

  const multiply = () => {
    setPrev(prev * current);
  }

  const subtract = () => {
    setPrev(prev - current);
  }

  const add = () => {
    setPrev(prev + current);
  }

  const calculate = () => {
    if (current && operator && prev) {
      setCalcCurrent(current);
      setCalcPrev(prev);
      setCalcOperator(operator);

      getOperation();

      setResult(prev);
      setCurrent(null);
      setOperator(null);
    }
  }

  const getOperation = () => {
    switch (operatorName) {
      case "add":
        add();
        break;
      case "subtract":
        subtract();
        break;
      case "multiply":
        multiply();
        break;
      case "divide":
        divide();
        break;
      default:
        break;
    }
  }

  const updateCurrent = (value) => {
    if (!operator) {
      setPrev(null);
    }
    setCurrent((current * 10) + value);
  }

  const saveCurrent = () => {
    setPrev(current);
  }

  const updateOperator = (value, title) => {

    setCalcCurrent(null);
    setCalcPrev(null);

    if (!prev) {
      saveCurrent();
    }

    setOperator(title);
    setOperatorName(value);

    if (current && prev) {
      getOperation();
    }

    console.log(prev);

    setCurrent(null);
  }

  const getHistory = () => {
    if (result && calcPrev && calcCurrent) {
      return calcPrev + " " + calcOperator + " " + calcCurrent + " = " + result;
    } else if (prev && operator) {
      return prev + " " + operator;
    } else {
      return null;
    }
  }

  const buttons = [
    { title: "C", value: 'C', cols: 3, action: reset },
    { title: "/", value: 'divide', cols: 1, action: updateOperator },
    { title: "7", value: 7, cols: 1, action: updateCurrent },
    { title: "8", value: 8, cols: 1, action: updateCurrent },
    { title: "9", value: 9, cols: 1, action: updateCurrent },
    { title: "x", value: 'multiply', cols: 1, action: updateOperator },
    { title: "4", value: 4, cols: 1, action: updateCurrent },
    { title: "5", value: 5, cols: 1, action: updateCurrent },
    { title: "6", value: 6, cols: 1, action: updateCurrent },
    { title: "-", value: 'subtract', cols: 1, action: updateOperator },
    { title: "1", value: 1, cols: 1, action: updateCurrent },
    { title: "2", value: 2, cols: 1, action: updateCurrent },
    { title: "3", value: 3, cols: 1, action: updateCurrent },
    { title: "+", value: 'add', cols: 1, action: updateOperator },
    { title: "0", value: 0, cols: 2, action: updateCurrent },
    { title: ".", value: '.', cols: 1, action: updateCurrent },
    { title: "=", value: 'calculate', cols: 1, action: calculate },
  ]

  return (
    <Fragment>
      <div className="d-flex justify-content-between align-items-center px-1">
        <h1>Calculator</h1>
        <div className="d-flex flex-column align-items-end">
          <div className="h5 text-muted">
            {getHistory()}
          </div>
          <div className="font-weight-bold h3">
            <span className="d-block">
              {current ? current : prev}
            </span>
          </div>
        </div>
      </div>
      <div className="d-flex flex-wrap">
        {buttons.map((button, key) => {
          return (
            <div key={key} className={`w-${25 * button.cols} p-1`}>
              <InputButton title={button.title} action={button.action} value={button.value} />
            </div>
          )
        })}
      </div>
    </Fragment>
  )
}

export default App;